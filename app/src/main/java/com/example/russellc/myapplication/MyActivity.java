package com.example.russellc.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;

import org.w3c.dom.Text;

import java.util.EventListener;
import java.util.ArrayList;


public class MyActivity extends Activity {
	private enum Operator {
		ADD,
		MINUS,
		MULTIPLY,
		DIVIDE
	}
	
    private static final String TAG = "MyActivity";
	private TextView displayView = null;
	private Float previousOperand = null;
	private Operator currentOperator = null;

    private void digitTapped(View v) {
		String buttonText = ((Button) v).getText().toString();
		
		this.displayView.setText(displayView.getText() + buttonText);
    }
	
	private void operationTapped(View v) {
		Float operand;
		try {
			operand = Float.parseFloat(this.displayView.getText().toString());
		} catch (NumberFormatException e) {
			return;
		}
		
		if (this.currentOperator != null) {
			Float ans = this.previousOperand;
			if (this.currentOperator == Operator.ADD) {
				ans += operand;
			} else if (this.currentOperator == Operator.MINUS) {
				ans -= operand;
			} else if (this.currentOperator == Operator.MULTIPLY) {
				ans *= operand;
			} else if (this.currentOperator == Operator.DIVIDE) {
				ans /= operand;
			}
			operand = ans;
			this.displayView.setText("");
		} else {
			this.displayView.setText("");
		}
		
		Operator selectedOperator = null;
		switch (v.getId()) {
			case R.id.activity_addButton:
				selectedOperator = Operator.ADD;
				break;
			case R.id.activity_minusButton:
				selectedOperator = Operator.MINUS;
				break;
			case R.id.activity_multiplyButton:
				selectedOperator = Operator.MULTIPLY;
				break;
			case R.id.activity_divideButton:
				selectedOperator = Operator.DIVIDE;
				break;
			case R.id.activity_equalsButton:
				this.displayView.setText(operand.toString());
				break;
			default:
				break;
		}
		this.currentOperator = selectedOperator;
		this.previousOperand = operand;
	}
	
	private void functionTapped(View v) {
		if (v.getId() == R.id.activity_clearButton) {
			this.displayView.setText("");
			this.currentOperator = null;
			this.previousOperand = null;
		}
	}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
		
		this.displayView = (TextView)findViewById(R.id.activity_displayView);

        View.OnClickListener buttonListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
				Object viewTag = v.getTag();
				if (viewTag != null) {
					if (viewTag.equals("digit"))
						MyActivity.this.digitTapped(v);
					else if (viewTag.equals("function"))
						MyActivity.this.functionTapped(v);
					else if (viewTag.equals("operation"))
						MyActivity.this.operationTapped(v);
				}
            }
        };

        ViewGroup digitsView = (ViewGroup)findViewById(R.id.activity_digitViewGroup);
        for (int i = 0; i < digitsView.getChildCount(); i++) {
            ViewGroup digitsRow = (ViewGroup)digitsView.getChildAt(i);
            for (int j = 0; j < digitsRow.getChildCount(); j++) {
                digitsRow.getChildAt(j).setOnClickListener(buttonListener);
            }
        }
		ViewGroup operationsCol = (ViewGroup)findViewById(R.id.activity_operationsLayout);
		for (int i = 0; i < operationsCol.getChildCount(); i++) {
			operationsCol.getChildAt(i).setOnClickListener(buttonListener);
		}
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
		switch (id) {
			case R.id.action_exit:
				finish();
				return true;
		}
        return super.onOptionsItemSelected(item);
    }
}
